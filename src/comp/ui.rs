use bevy::prelude::*;

/// Marker for displaying player's health
#[derive(Debug, Default, Properties)]
pub struct PlayerHealthDisplay;

/// Marker for displaying player's health bar
#[derive(Debug, Default, Properties)]
pub struct PlayerHealthBarDisplay;

/// Marker for displaying player's energy
#[derive(Debug, Default, Properties)]
pub struct PlayerEnergyDisplay;

/// Marker for displaying player's energy bar
#[derive(Debug, Default, Properties)]
pub struct PlayerEnergyBarDisplay;

/// Marker for displaying player's shields
#[derive(Debug, Default, Properties)]
pub struct PlayerShieldsDisplay;

/// Marker for displaying player's shields bar
#[derive(Debug, Default, Properties)]
pub struct PlayerShieldsBarDisplay;

/// Marker for displaying player's shield configuration
#[derive(Debug, Default, Properties)]
pub struct PlayerShieldConfigDisplay;

/// Marker for displaying player's score
#[derive(Debug, Default, Properties)]
pub struct PlayerScoreDisplay;

/// Marker for the Game Over display
#[derive(Debug, Default, Properties)]
pub struct GameOverDisplay;
