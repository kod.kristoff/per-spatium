use bevy::prelude::*;

/// This component causes damage.
///
/// TODO: Maybe split this into components for each damage type when implemented?
#[derive(Debug, Default, Properties)]
pub struct Damage {
    pub hull: i32,
    pub shields: i32,
}

/// This component provides information about the source/origin of the damage,
/// used to prevent "my projectile hit me" scenarios.
///
/// TODO: Maybe add groups for friendly-fire scenarios?
/// Or maybe even add such functionality to collision system?
#[derive(Debug, Properties)]
pub struct DamageOrigin {
    pub entity: Entity,
}

/// This component marks entity as a subject of death.
///
/// TODO: Move this to [HealthStats] or something?
/// I am not sure when the event updates are handled.
/// I believe that if `is_dead` is true, any POST_UPDATE systems
/// can react with things like actual component removal or
/// "particles on death". This design will need a review eventually.
#[derive(Debug, Default, Properties)]
pub struct CanDie {
    pub is_dead: bool,
}

/// Helper constant to simulate "particle type".
pub const DEATH_EFFECT_PARTICLES_ASTEROID: &str = "asteroid";

/// This component makes entity to produce particles on death.
#[derive(Debug, Properties)]
pub struct DeathEffectParticles {
    pub effect: String,
}

/// This component represents entity's health.
#[derive(Debug, Default, Properties)]
pub struct HealthStats {
    pub hull: i32,
    pub max_hull: i32,
}

/// This component represents movement stats.
#[derive(Debug, Default, Properties)]
pub struct MovementSpeed {
    pub accel: f32,
    pub max: f32,
}

/// This component represents entity's energy.
#[derive(Debug, Default, Properties)]
pub struct EnergyStats {
    pub current: i32,
    pub max: i32,
}

/// This component represents entity's shields.
#[derive(Debug, Default, Properties)]
pub struct ShieldStats {
    pub current: i32,
    pub max: i32,
}

/// This component represents entity's shields configuration/control.
#[derive(Debug, Default, Properties)]
pub struct ShieldConfiguration {
    /// How much shields can be created from 1 energy
    pub shields_per_energy: f32,
    /// How much energy is being converted into shields.
    /// 0 effectively means that shields are off.
    pub effectivity: f32,
    /// Last calculated energy drain.
    ///
    /// TODO: Move this into something like TmpShieldConfiguration,
    /// to limit mut accesses.
    pub last_energy_drain: f32,
}

/// This component represents entity's shields configuration/control
/// temporary values. This limits Mut accesses to other components.
///
/// TODO: Better name :)
#[derive(Debug, Default, Properties)]
pub struct TmpShieldConfiguration {
    /// How much energy is being converted into shields.
    /// This number is "final" efficiency, after both
    /// controlled regulation and energy deficit
    pub real_efficiency: f32,
    /// Last calculated energy drain.
    ///
    /// TODO: Move this into something like TmpShieldConfiguration,
    /// to limit mut accesses.
    pub last_energy_drain: f32,
}

/// This component allows entity to regenerate energy.
///
/// TODO: Maybe move the `accumulator` to separate component, to limit the amount of Mut accesses
#[derive(Debug, Default, Properties)]
pub struct EnergyRegen {
    pub generation: f32,
    pub drain: f32,
    pub accumulator: f32,
    /// When energy drain is greater than generation,
    /// efficiency can be used to determine the ratio generation / drain,
    /// providing information for "throttling" other systems, like shields.
    pub efficiency: f32,
    /// Whether the accumulator was decrementing or incrementing last tick.
    pub last_acc_sign: bool,
}

/// This component allows entity to regenerate shields.
///
/// TODO: Maybe move the `accumulator` to separate component, to limit the amount of Mut accesses
#[derive(Debug, Default, Properties)]
pub struct ShieldRegen {
    pub generation: f32,
    pub drain: f32,
    pub accumulator: f32,
    /// Whether the accumulator was decrementing or incrementing last tick.
    pub last_acc_sign: bool,
}

/// Component providing information that entity dissipates
/// and it will stop existing after some time.
///
/// NOTE: It might die sooner, if the dissipation would "damage" the entity
#[derive(Debug, Default, Properties)]
pub struct Dissipates {
    pub timer: Timer,
}

/// Component providing information that entity dies after it **is hit**
/// one or more times (defined by hits_to_die)
///
/// TODO: Maybe put this into [HealthStats]??
#[derive(Debug, Default, Properties)]
pub struct DiesOnHit {
    pub hits_to_die: i32,
}

/// This component allows entity to collect [Collectible] entities on touch.
#[derive(Debug, Default, Properties)]
pub struct Collector;

/// This component makes entity collectible.
///
/// `collected` is flag for post-pickup systems.
#[derive(Debug, Default, Properties)]
pub struct Collectible {
    pub collected: bool,
}

/// This component affects [MovementSpeed] stats of entities.
#[derive(Debug, Default, Properties)]
pub struct SpeedPowerUp {
    pub speed: MovementSpeed,
}

/// This component affects [HealthStats] stats of entities.
#[derive(Debug, Default, Properties)]
pub struct HealthPowerUp {
    pub health: HealthStats,
}

/// This component affects [EnergyStats] stats of entities.
#[derive(Debug, Default, Properties)]
pub struct EnergyPowerUp {
    pub energy: EnergyStats,
}

/// This component affects [ShieldStats] stats of entities.
#[derive(Debug, Default, Properties)]
pub struct ShieldPowerUp {
    pub shield: ShieldStats,
}

/// Represents that an entity has a score.
#[derive(Debug, Default, Properties)]
pub struct Score {
    pub score: i32,
}
