use crate::comp::actor;
use crate::comp::physics;
use crate::comp::stats;
use crate::res;
use crate::spawn;

use bevy::prelude::*;

/// Plugin bringing in mostly stats manipulating systems and resources
pub struct GameStatsPlugin;

impl Plugin for GameStatsPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_event::<res::DamageEvent>()
            .add_event::<res::PickupEvent>()
            .add_event::<res::KillEvent>()
            .init_resource::<res::ContactDamageListenerState>()
            .init_resource::<res::ContactPickupListenerState>()
            .init_resource::<res::OnHitDieState>()
            .init_resource::<res::OnKillScoreState>()
            .init_resource::<res::ApplySpeedPowerUpState>()
            .init_resource::<res::ApplyHealthPowerUpState>()
            .init_resource::<res::ApplyEnergyPowerUpState>()
            .init_resource::<res::ApplyShieldPowerUpState>()
            .add_system_to_stage(stage::PRE_UPDATE, contact_damage_system.system())
            .add_system(detect_pickup_system.system())
            .add_system(timed_dissipation_system.system())
            .add_system(apply_speed_powerup_system.system())
            .add_system(apply_health_powerup_system.system())
            .add_system(apply_energy_powerup_system.system())
            .add_system(apply_shield_powerup_system.system())
            .add_system(regen_energy_system.system())
            .add_system(regen_shields_system.system())
            .add_system(shields_energy_system.system())
            .add_system_to_stage(stage::POST_UPDATE, collectible_cleanup_system.system())
            .add_system_to_stage(stage::POST_UPDATE, on_hit_die_system.system())
            .add_system_to_stage(stage::POST_UPDATE, dead_cleanup_system.system())
            .add_system_to_stage(stage::POST_UPDATE, on_kill_score_system.system())
            .add_system_to_stage(stage::POST_UPDATE, death_effect_particles_system.system());
    }
}

/// Processes damage on contact of two entities.
///
/// TODO: Make it less terrific, ideally by only querying components
/// of collided entities, without iterating over entities.
pub fn contact_damage_system(
    mut damage_events: ResMut<Events<res::DamageEvent>>,
    mut kill_events: ResMut<Events<res::KillEvent>>,
    collision_events: Res<Events<res::CollisionEvent>>,
    mut collision_event_reader: ResMut<res::ContactDamageListenerState>,
    mut damages: Query<(Entity, &stats::Damage)>,
    mut damageables: Query<(Entity, &mut stats::HealthStats, &mut stats::CanDie)>,
    damage_sources: Query<(Entity, &stats::DamageOrigin)>,
    shielded_damageables: Query<(Entity, &mut stats::ShieldStats)>,
) {
    for event in collision_event_reader.event_reader.iter(&collision_events) {
        for (src_ent, damage) in &mut damages.iter() {
            if src_ent == event.0 || src_ent == event.1 {
                for (hit_ent, mut health, mut death) in &mut damageables.iter() {
                    if (hit_ent == event.0 || hit_ent == event.1) && hit_ent != src_ent {
                        // TODO: For reasons unknown to me, this query fails with `CannotReadArchetype` the
                        // first time if a projectile is fired AND this system runs in the UPDATE stage.
                        // After the first time, it works as expected, however.
                        // I moved this system into PRE_UPDATE because of this
                        let dmg_source = damage_sources.get::<stats::DamageOrigin>(src_ent);
                        let mut source_exists = None;
                        let hit_origin = if let Ok(origin) = dmg_source {
                            source_exists = Some(origin.entity);
                            hit_ent == origin.entity
                        } else {
                            false
                        };
                        if !hit_origin {
                            // Little debug info
                            //println!("Damage detected!");
                            // Check for shielding
                            let shielded =
                                shielded_damageables.get_mut::<stats::ShieldStats>(hit_ent);
                            let mut shield_absorbed = 0.0;
                            if let Ok(mut shields) = shielded {
                                if shields.current <= damage.shields {
                                    // check if there were shields before (to prevent zero-div)
                                    // as shield damage could be 0 and current shields too, triggering the div.
                                    if shields.current > 0 {
                                        // shielding absorbed some part of the damage
                                        shield_absorbed =
                                            shields.current as f32 / damage.shields as f32;
                                    }
                                    shields.current = 0;
                                } else {
                                    shields.current -= damage.shields;
                                    // shielding absorbed everything
                                    shield_absorbed = 1.0;
                                }
                            }
                            // converting to damage reduction
                            shield_absorbed = 1.0 - shield_absorbed;
                            // Apply damage absorption and damage health/hull, potentially killing the target.
                            if health.hull <= (damage.hull as f32 * shield_absorbed).ceil() as i32 {
                                health.hull = 0;
                                damage_events.send(res::DamageEvent::new(src_ent, hit_ent));
                                // verify execution flow of this
                                if let Some(source) = source_exists {
                                    if !death.is_dead {
                                        kill_events.send(res::KillEvent::new(source, hit_ent))
                                    }
                                };
                                death.is_dead = true;
                            } else {
                                health.hull -= (damage.hull as f32 * shield_absorbed).ceil() as i32;
                                damage_events.send(res::DamageEvent::new(src_ent, hit_ent));
                            }
                        }
                    }
                }
            }
        }
    }
}

/// Increases dissipation timer on entities that Dissipate
/// and removes the entity if the timer finishes.
pub fn timed_dissipation_system(
    mut commands: Commands,
    time: Res<Time>,
    mut query: Query<(Entity, &mut stats::Dissipates)>,
) {
    for (ent, mut dissipates) in &mut query.iter() {
        if dissipates.timer.finished {
            commands.despawn_recursive(ent);
        } else {
            dissipates.timer.tick(time.delta_seconds);
        }
    }
}

/// Despawns `DiesOnHit` entities that hitted anyting a certain number of times
/// and actualizes this count.
///
/// TODO: Maybe let this system live next to / in [contact_damage_system],
/// removing the `DamageEvent` dependancy?
///
/// TODO: If the entity `CanDie`, should it mark the entity "dead" instead of despawning?
pub fn on_hit_die_system(
    mut commands: Commands,
    damage_events: ResMut<Events<res::DamageEvent>>,
    mut state: ResMut<res::OnHitDieState>,
    mut query: Query<(Entity, &mut stats::DiesOnHit)>,
) {
    for event in state.event_reader.iter(&damage_events) {
        for (ent, mut hitted) in &mut query.iter() {
            if ent == event.src {
                if hitted.hits_to_die - 1 <= 0 {
                    commands.despawn_recursive(ent);
                } else {
                    hitted.hits_to_die -= 1;
                }
            }
        }
    }
}

/// Detects pickup on contact of two entities.
///
/// TODO: Make it less terrific, ideally by only querying components
/// of collided entities, without iterating over entities.
pub fn detect_pickup_system(
    mut pickup_events: ResMut<Events<res::PickupEvent>>,
    collisions: Res<Events<res::CollisionEvent>>,
    mut collision_reader: ResMut<res::ContactPickupListenerState>,
    mut collectors: Query<(Entity, &stats::Collector)>,
    mut pickups: Query<(Entity, &mut stats::Collectible)>,
) {
    for event in collision_reader.event_reader.iter(&collisions) {
        for (collector_ent, _) in &mut collectors.iter() {
            if collector_ent == event.0 || collector_ent == event.1 {
                for (pickup_ent, mut pickup) in &mut pickups.iter() {
                    if (pickup_ent == event.0 || pickup_ent == event.1)
                        && pickup_ent != collector_ent
                    {
                        // Little debug info
                        //println!("Pickup detected!");
                        pickup_events.send(res::PickupEvent::new(collector_ent, pickup_ent));
                        pickup.collected = true;
                    }
                }
            }
        }
    }
}

/// Applies "Speed" PowerUp effects.
pub fn apply_speed_powerup_system(
    pickups: ResMut<Events<res::PickupEvent>>,
    mut pickup_reader: ResMut<res::ApplySpeedPowerUpState>,
    mut powerups: Query<(Entity, &stats::SpeedPowerUp)>,
    mut collectors: Query<(Entity, &mut stats::MovementSpeed)>,
) {
    for event in pickup_reader.event_reader.iter(&pickups) {
        for (collector_ent, mut speed) in &mut collectors.iter() {
            if collector_ent == event.collector {
                for (pickup_ent, powerup) in &mut powerups.iter() {
                    if pickup_ent == event.pickup {
                        speed.accel += powerup.speed.accel;
                        speed.max += powerup.speed.max;
                    }
                }
            }
        }
    }
}

/// Applies "Health" PowerUp effects.
pub fn apply_health_powerup_system(
    pickups: ResMut<Events<res::PickupEvent>>,
    mut pickup_reader: ResMut<res::ApplyHealthPowerUpState>,
    mut powerups: Query<(Entity, &stats::HealthPowerUp)>,
    mut collectors: Query<(Entity, &mut stats::HealthStats)>,
) {
    for event in pickup_reader.event_reader.iter(&pickups) {
        for (collector_ent, mut stats) in &mut collectors.iter() {
            if collector_ent == event.collector {
                for (pickup_ent, powerup) in &mut powerups.iter() {
                    if pickup_ent == event.pickup {
                        stats.max_hull += powerup.health.max_hull;
                        if stats.hull + powerup.health.hull > stats.max_hull {
                            stats.hull = stats.max_hull;
                        } else {
                            stats.hull += powerup.health.hull;
                        }
                    }
                }
            }
        }
    }
}

/// Applies "Energy" PowerUp effects.
pub fn apply_energy_powerup_system(
    pickups: ResMut<Events<res::PickupEvent>>,
    mut pickup_reader: ResMut<res::ApplyEnergyPowerUpState>,
    mut powerups: Query<(Entity, &stats::EnergyPowerUp)>,
    mut collectors: Query<(Entity, &mut stats::EnergyStats)>,
) {
    for event in pickup_reader.event_reader.iter(&pickups) {
        for (collector_ent, mut stats) in &mut collectors.iter() {
            if collector_ent == event.collector {
                for (pickup_ent, powerup) in &mut powerups.iter() {
                    if pickup_ent == event.pickup {
                        stats.max += powerup.energy.max;
                        if stats.current + powerup.energy.current > stats.max {
                            stats.current = stats.max;
                        } else {
                            stats.current += powerup.energy.current;
                        }
                    }
                }
            }
        }
    }
}

/// Applies "Shield" PowerUp effects.
pub fn apply_shield_powerup_system(
    pickups: ResMut<Events<res::PickupEvent>>,
    mut pickup_reader: ResMut<res::ApplyShieldPowerUpState>,
    mut powerups: Query<(Entity, &stats::ShieldPowerUp)>,
    mut collectors: Query<(Entity, &mut stats::ShieldStats)>,
) {
    for event in pickup_reader.event_reader.iter(&pickups) {
        for (collector_ent, mut stats) in &mut collectors.iter() {
            if collector_ent == event.collector {
                for (pickup_ent, powerup) in &mut powerups.iter() {
                    if pickup_ent == event.pickup {
                        stats.max += powerup.shield.max;
                        if stats.current + powerup.shield.current > stats.max {
                            stats.current = stats.max;
                        } else {
                            stats.current += powerup.shield.current;
                        }
                    }
                }
            }
        }
    }
}

/// Periodically changes EnergyStats if entity has EnergyRegen.
/// Also recomputes the efficiency of energy usage.
///
/// TODO: The `efficiency` might be calculated elsewhere/differently,
/// like on change of `generation` and/or `drain`
pub fn regen_energy_system(
    time: Res<Time>,
    mut query: Query<(&mut stats::EnergyStats, &mut stats::EnergyRegen)>,
) {
    for (mut stats, mut regen) in &mut query.iter() {
        // if energy changed its flow (gain/lost), change accumulator sign
        if (regen.generation - regen.drain).is_sign_positive() != regen.last_acc_sign {
            regen.last_acc_sign = !regen.last_acc_sign;
            regen.accumulator = regen.accumulator * -1.0;
        }
        let delta_change = time.delta_seconds * (regen.generation - regen.drain);
        regen.accumulator += delta_change;
        let acc_int = if regen.accumulator > 0.0 {
            regen.accumulator.floor()
        } else {
            regen.accumulator.ceil()
        };
        if delta_change > 0.0 {
            // Energy is being generated
            if acc_int as i32 + stats.current > stats.max {
                stats.current = stats.max;
            } else {
                stats.current += acc_int as i32;
            }
            if acc_int > 0.0 {
                regen.accumulator %= acc_int;
            }
            // there is enough energy (as it is being restored), so no efficiency penalty occurs
            regen.efficiency = 1.0;
        } else if delta_change < 0.0 {
            // Energy is being drained
            if acc_int as i32 + stats.current <= 0 {
                stats.current = 0;
                // Energy is depleted, there will be an efficiency penalty.
                // NOTE: This should never end up as x / 0, as drain is non-zero here
                // Or as long as no negative numbers creep into `generation` :/
                // Also, the efficiency should be less than 1.
                regen.efficiency = regen.generation / regen.drain
            } else {
                stats.current += acc_int as i32;
                // Loosing energy, but there is still some in reserve
                regen.efficiency = 1.0;
            }
            if acc_int < 0.0 {
                regen.accumulator %= acc_int;
            }
        } else if regen.generation == regen.drain {
            // There is a perfect balance in `generation` and `drain`, so no penalty
            regen.efficiency = 1.0;
        }
    }
}

/// Periodically changes ShieldStats if entity has ShieldRegen.
///
/// TODO: Currently, `TmpShieldConfiguration` is also optionally queried,
/// to allow shield control / energy lack. This part of the system might be moved away though,
/// splitting the system into `With<TmpShieldConfiguration>` and `Without` systems.
pub fn regen_shields_system(
    time: Res<Time>,
    mut query: Query<(Entity, &mut stats::ShieldStats, &mut stats::ShieldRegen)>,
    configs: Query<&stats::TmpShieldConfiguration>,
) {
    for (ent, mut stats, mut regen) in &mut query.iter() {
        // query for shield configuration
        let config = if let Ok(config) = configs.get::<stats::TmpShieldConfiguration>(ent) {
            config.real_efficiency
        } else {
            // no config, so full efficiency
            1.0
        };
        // if energy changed its flow (gain/lost), change accumulator sign
        if (regen.generation - regen.drain).is_sign_positive() != regen.last_acc_sign {
            regen.last_acc_sign = !regen.last_acc_sign;
            regen.accumulator = regen.accumulator * -1.0;
        }
        let delta_change = time.delta_seconds * (regen.generation * config - regen.drain);
        regen.accumulator += delta_change;
        let acc_int = if regen.accumulator > 0.0 {
            regen.accumulator.floor()
        } else {
            regen.accumulator.ceil()
        };
        if delta_change > 0.0 {
            // Shields are being charged
            if acc_int as i32 + stats.current > stats.max {
                stats.current = stats.max;
            } else {
                stats.current += acc_int as i32;
            }
            if acc_int > 0.0 {
                regen.accumulator %= acc_int;
            }
        } else if delta_change < 0.0 {
            // Shields are being drained
            if acc_int as i32 + stats.current < 0 {
                stats.current = 0;
            } else {
                stats.current += acc_int as i32;
            }
            if acc_int < 0.0 {
                regen.accumulator %= acc_int;
            }
        }
    }
}

/// Alters EnergyRegen based on ShieldConfiguration and ShieldRegen.
///
/// TODO: Review information ownership. Should be ShieldRegen needed here?
/// Should be the ShieldConfiguration borrowed mutably?
pub fn shields_energy_system(
    mut query: Query<(
        &mut stats::EnergyRegen,
        &stats::ShieldRegen,
        &stats::ShieldConfiguration,
        &mut stats::TmpShieldConfiguration,
    )>,
) {
    for (mut energy, shields, config, mut tmp_config) in &mut query.iter() {
        // First, calculate real efficiency based on shield regulation and energy deficit.
        // this is used in shield generation.
        tmp_config.real_efficiency = energy.efficiency * config.effectivity;
        // Now, change requested energy consumption (based on the shield regulation, but not energy deficit)
        let new_energy_drain =
            (shields.generation / config.shields_per_energy) * config.effectivity;
        if new_energy_drain != tmp_config.last_energy_drain {
            // Restore original (base) drain
            energy.drain -= tmp_config.last_energy_drain;
            // Remember the new drain (to be able to revert it)
            tmp_config.last_energy_drain = new_energy_drain;
            // Apply the new drain
            energy.drain += new_energy_drain;
        }
    }
}

/// Removes `collected` `Collectibles`.
///
/// TODO: Check execution order, for it might be possible that
/// the `Collectible` gets removed before it's powerups get applied.
pub fn collectible_cleanup_system(
    mut commands: Commands,
    mut collectibles: Query<(Entity, &stats::Collectible)>,
) {
    for (ent, collectible) in &mut collectibles.iter() {
        if collectible.collected {
            commands.despawn_recursive(ent);
        }
    }
}

/// Removes `is_dead` entities that `CanDie`.
///
/// TODO: Check execution order, review the `CanDie` component
/// and entity de/spawning in general.
pub fn dead_cleanup_system(mut commands: Commands, mut deaths: Query<(Entity, &stats::CanDie)>) {
    for (ent, death) in &mut deaths.iter() {
        if death.is_dead {
            commands.despawn_recursive(ent);
        }
    }
}

/// Produces particles on death of entities that `CanDie`
/// and have `DeathEffectParticles`.
pub fn death_effect_particles_system(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    //materials: Res<res::ColorMaterialStorage>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut deaths: Query<(&stats::CanDie, &stats::DeathEffectParticles, &Transform)>,
) {
    for (death, effect, transform) in &mut deaths.iter() {
        if death.is_dead {
            if effect.effect == stats::DEATH_EFFECT_PARTICLES_ASTEROID {
                let mut rng = rand::thread_rng();
                use rand::Rng;

                for i in 0..5 {
                    let vx: f32 = rng.gen_range(-40.0, 40.0);
                    let vy: f32 = rng.gen_range(-40.0, 40.0);

                    let vel = physics::Velocity(Vec2::new(vx, vy));
                    let mut translation = transform.translation();
                    // to fix the broken transparency, so the particles do not have visible rectangle bounds around them
                    translation.set_z(i as f32 / 100.0);
                    let data = spawn::AsteroidParticleDataComponents {
                        transform: Transform::from_translation(translation),
                        velocity: vel,
                        ..Default::default()
                    };

                    spawn::spawn_asteroid_particle(
                        &mut commands,
                        &asset_server,
                        &mut materials,
                        data,
                    );
                }
            }
        }
    }
}

/// Increments player score on death of entity with `Score`.
///
/// TODO: Review the `KillEvent` and the `CanDie` component.
/// I suspect there are some race conditions here.
pub fn on_kill_score_system(
    events: Res<Events<res::KillEvent>>,
    mut state: ResMut<res::OnKillScoreState>,
    mut deaths: Query<(Entity, &stats::CanDie, &stats::Score)>,
    mut players: Query<(Entity, &mut actor::Player)>,
) {
    for event in state.event_reader.iter(&events) {
        for (dead_ent, death, score) in &mut deaths.iter() {
            if dead_ent == event.targ && death.is_dead {
                for (killer_ent, mut player) in &mut players.iter() {
                    if killer_ent == event.src {
                        player.score += score.score;
                    }
                }
            }
        }
    }
}
